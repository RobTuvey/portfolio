import { useIsMobile } from '.';
import { desktopTheme, mobileTheme } from '../Util';

export function useTheme() {
    const { isMobile } = useIsMobile();

    return isMobile ? mobileTheme : desktopTheme;
}