import React from 'react';
import { SectionContainer } from '../../../Components';
import { Typography } from '@mui/material';

export function Forfeit() {
    return (
        <SectionContainer style={{ margin: '25px 0px' }}>
            <Typography variant="h4">Forfeit</Typography>
        </SectionContainer>
    );
}
