import React from 'react';
import { SectionContainer } from '../../Components';
import { Typography } from '@mui/material';
import { Forfeit } from './Forfeit';

export function Projects() {
    return (
        <SectionContainer>
            <Typography variant="h3">Projects</Typography>
            <Forfeit />
        </SectionContainer>
    );
}
