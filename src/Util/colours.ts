export const Colours = {
    background: '#3D393C',
    primary: '#CC6235',
    secondary: '#9B9693',
    accent: '#E3BF86',
    text: '#C3B59F',
};
